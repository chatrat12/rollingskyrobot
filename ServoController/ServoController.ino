#include <Servo.h>

bool potMode = false;

Servo _servo;
int potPin = 2;
float scale = 180.0f / 1024.0f;

void setup() 
{
  _servo.attach(9);
  _servo.write(90);
  Serial.begin(115200);
  //Serial.println("Reporting for duty");

}

void loop() 
{
	if (potMode)
		ServoMirroPot();
	else
	{
		if (Serial.available() > 0)
		{
			
				auto angle = Serial.read();
				_servo.write(180-angle);
			
		}
	}
}


void ServoMirroPot()
{
	auto value = analogRead(potPin) * scale;
	_servo.write((int)value);
	Serial.println(value);
}

void WaitForSerial(int bufferSize)
{
	while(Serial.available() < bufferSize){}
}
