#include <SoftwareSerial.h>
#include <Servo8Bit.h>

int ledPin = 0;
constexpr int rxPin = 3;
constexpr int txPin = 1;

SoftwareSerial _serial = SoftwareSerial(rxPin, txPin);
bool ledOn = true;

void setup() 
{
  pinMode(ledPin, OUTPUT);
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  _serial.begin(9600);
  digitalWrite(ledPin, ledOn);
}

void loop() 
{
  bool toggled = false;
  while(_serial.available() > 0)
  {
    if(!toggled)
    {
      toggled = true;
      ledOn = !ledOn;
      digitalWrite(ledPin, ledOn);
    }
    _serial.read();
  }
  _serial.println("Serial!");
  
}
