#include <SoftwareSerial.h>
#include "Servo8Bit.h"

constexpr int servoPin = 0;
constexpr int rxPin = 3;
constexpr int txPin = 1;

SoftwareSerial _serial = SoftwareSerial(rxPin, txPin);
Servo8Bit _servo;

int _angle;
long _lastUpdateTime = 0;
byte _lastCount = 0;
long _timer = 0;
bool _servoPinOn = false;
int _cutOffTime = 0;

long _pulseWidth = 10000;
long _w2 = _pulseWidth / 2;
long _minCutOffTime = _pulseWidth / 20;

void setup() 
{ 
  //TCCR1 = 0x00000100;
  pinMode(servoPin, OUTPUT);
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  _serial.begin(9600);
  //SetAngle(90);
  _servo.attach(0);
  _servo.write(90);
}

void loop() 
{
  while(_serial.available() > 0)
  {
    //SetAngle(_serial.read());
  }
  //_serial.println(String(TCNT1));
  //UpdateServo();
}

float microSecondsPerDegree = (float)_minCutOffTime / 180.0f;
void UpdateServo()
{
  if(_lastCount > TCNT1)
    _timer += 255 - _lastCount + TCNT1;
    else
      _timer += TCNT1 - _lastCount;
  //_timer += TCNT1 - _lastUpdateTime;

  if(_servoPinOn && _timer > _w2)
  {
    setServoState(false);
  }

  if(_timer > _pulseWidth)
  {
    _timer = 0;
    if(_angle > 0)
      setServoState(true);
  }

  _lastCount = TCNT1;
  //_lastUpdateTime = micros();
}

void SetAngle(int angle)
{
  _cutOffTime =_minCutOffTime + (long)(microSecondsPerDegree * angle);
  _angle = angle;
}

void setServoState(bool state)
{
  digitalWrite(servoPin, state);
  _servoPinOn = state;
}

