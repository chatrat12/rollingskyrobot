﻿using System;
using System.IO.Ports;

namespace UnoProgrammingTool
{
    public class SerialCommunicator
    {
        public delegate void MessageDelegate(string message);
        public event MessageDelegate LoggedMessage;

        public bool IsOpen { get { return _serial.IsOpen; } }

        private SerialPort _serial;

        public SerialCommunicator(string portName, int baud)
        {
            _serial = new SerialPort();
            _serial.PortName = portName;
            _serial.BaudRate = baud;

            _serial.ReadTimeout = 500;
            _serial.WriteTimeout = 500;

            //_serial.Open();
            _serial.DataReceived += _serial_DataReceived;
        }
        public void SetServo(byte angle)
        {
            SendData(angle);
        }
        public void SendData(params byte[] data)
        {
            _serial.Write(data, 0, data.Length);
        }

        private void _serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = _serial.ReadByte();
            if(LoggedMessage != null)
            {
                LoggedMessage(data.ToString());
            }
        }
    }
}
