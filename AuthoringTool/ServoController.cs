﻿using System.IO.Ports;

namespace UnoProgrammingTool
{
    public class ServoController
    {
        public ServoStatus ServoStatus { get; private set; } = new ServoStatus();
        SerialPort _serial;
        private float _previousAngle;

        public ServoController(SerialPort serial)
        {
            _serial = serial;
            ServoStatus.AngleUpdated += delegate (object sender, System.EventArgs e)
            {
                WriteToSerial();
                _previousAngle = ServoStatus.Angle;
            };
        }

        public void WriteToSerial()
        {
            if(_serial.IsOpen && ServoStatus.Angle != _previousAngle)
            {
                SetServo((byte)(MathHelper.RoundToInt(ServoStatus.Angle) + 90));
            }
        }

        public void SetServo(byte angle)
        {
            SendData(angle);
        }
        public void SendData(params byte[] data)
        {
            _serial.Write(data, 0, data.Length);
        }
    }
}
