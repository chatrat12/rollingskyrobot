﻿namespace UnoProgrammingTool
{
    partial class AuthoringToolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthoringToolForm));
            this._comPanel = new UnoProgrammingTool.Controls.COMPanel();
            this._playthroughEditor = new UnoProgrammingTool.PlaythroughEditor();
            this._toolStrip = new UnoProgrammingTool.Controls.TopBarMenu();
            this._servoPanel = new UnoProgrammingTool.Controls.ServoPanel();
            this._editorControls = new UnoProgrammingTool.Controls.EditorControls();
            this.SuspendLayout();
            // 
            // comPanel1
            // 
            this._comPanel.Location = new System.Drawing.Point(18, 27);
            this._comPanel.Name = "comPanel1";
            this._comPanel.Size = new System.Drawing.Size(244, 103);
            this._comPanel.TabIndex = 24;
            this._comPanel.TabStop = false;
            // 
            // _playthroughEditor
            // 
            this._playthroughEditor.Location = new System.Drawing.Point(268, 27);
            this._playthroughEditor.Name = "_playthroughEditor";
            this._playthroughEditor.Size = new System.Drawing.Size(400, 695);
            this._playthroughEditor.Snapping = true;
            this._playthroughEditor.TabIndex = 21;
            this._playthroughEditor.TabStop = false;
            this._playthroughEditor.Time = 0D;
            // 
            // _toolStrip
            // 
            this._toolStrip.File = null;
            this._toolStrip.Location = new System.Drawing.Point(0, 0);
            this._toolStrip.Name = "_toolStrip";
            this._toolStrip.Size = new System.Drawing.Size(682, 24);
            this._toolStrip.TabIndex = 25;
            this._toolStrip.Text = "Menu";
            // 
            // servoPanel1
            // 
            this._servoPanel.Location = new System.Drawing.Point(18, 136);
            this._servoPanel.Name = "servoPanel1";
            this._servoPanel.Size = new System.Drawing.Size(241, 184);
            this._servoPanel.TabIndex = 26;
            // 
            // _editorControls
            // 
            this._editorControls.Editor = null;
            this._editorControls.Location = new System.Drawing.Point(18, 326);
            this._editorControls.Name = "_editorControls";
            this._editorControls.Size = new System.Drawing.Size(241, 164);
            this._editorControls.TabIndex = 27;
            // 
            // AuthoringToolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 736);
            this.Controls.Add(this._editorControls);
            this.Controls.Add(this._servoPanel);
            this.Controls.Add(this._comPanel);
            this.Controls.Add(this._playthroughEditor);
            this.Controls.Add(this._toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._toolStrip;
            this.Name = "AuthoringToolForm";
            this.Text = "Playthrough Authoring Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private PlaythroughEditor _playthroughEditor;
        private Controls.COMPanel _comPanel;
        private Controls.TopBarMenu _toolStrip;
        private Controls.ServoPanel _servoPanel;
        private Controls.EditorControls _editorControls;
    }
}

