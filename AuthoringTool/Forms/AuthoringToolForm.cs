﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace UnoProgrammingTool
{
    public partial class AuthoringToolForm : Form
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct NativeMessage
        {
            public IntPtr Handle;
            public uint Message;
            public IntPtr WParameter;
            public IntPtr LParameter;
            public uint Time;
            public Point Location;
        }

        [DllImport("user32.dll")]
        public static extern int PeekMessage(out NativeMessage message, IntPtr window, uint filterMin, uint filterMax, uint remove);

        private ServoController _servoController;

        private Stopwatch _stopWatch = new Stopwatch();
        private double _previousFrameTime;

        private PlaythroughFile _file = new PlaythroughFile();

        public AuthoringToolForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _playthroughEditor.SetPlaythroughFile(_file);
            _editorControls.Editor = _playthroughEditor;
            _toolStrip.File = _file;

            _servoController = new ServoController(_comPanel.Serial);
            _servoController.ServoStatus.MinAngle = Properties.Settings.Default.MinServo;
            _servoController.ServoStatus.MaxAngle = Properties.Settings.Default.MaxServo;
            _servoPanel.ServoStatus = _servoController.ServoStatus;

            _playthroughEditor.Stopped += delegate 
            {
                var servoStatus = _servoController.ServoStatus;
                servoStatus.Angle = MathHelper.Lerp(servoStatus.MinAngle, servoStatus.MaxAngle, 0.5f);
            };

            _stopWatch.Start();
            Application.Idle += delegate
            {
                while (IsApplicationIdle())
                {
                    if (_stopWatch.Elapsed.TotalSeconds - _previousFrameTime > 0.01)
                    {
                        var currentTime = _stopWatch.Elapsed.TotalSeconds;
                        var deltaTime = currentTime - _previousFrameTime;
                        UpdateLoop(deltaTime);
                        _previousFrameTime = currentTime;
                    }
                }
            };
        }
        

        private void UpdateLoop(double deltaTime)
        {
            var servoStatus = _servoController.ServoStatus;
            _playthroughEditor.Update(deltaTime);
            if(_playthroughEditor.Playing)
            {
                var alpha = _file.Playthrough.Evaluate((float) _playthroughEditor.Time);
                servoStatus.Angle = MathHelper.Lerp(servoStatus.MinAngle, servoStatus.MaxAngle, alpha);
            }
        }


        bool IsApplicationIdle()
        {
            NativeMessage result;
            return PeekMessage(out result, IntPtr.Zero, (uint)0, (uint)0, (uint)0) == 0;
        }
    }
}
