﻿
using System.IO;

namespace UnoProgrammingTool
{
    public static class PlaythroughDeserializer
    {
        public static Playthrough Dersialize(byte[] bytes)
        {
            var result = new Playthrough();
            result.Keys.Clear();
            BinaryReader reader = new BinaryReader(new MemoryStream(bytes));
            var version = reader.ReadInt32();
            var keyCount = reader.ReadInt32();
            for (int i = 0; i < keyCount; i++)
            {
                result.Keys.Add(new Key(reader.ReadSingle(), reader.ReadSingle()));
            }
            return result;
        }
    }
}
