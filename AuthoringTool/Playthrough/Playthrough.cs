﻿using System.Collections.Generic;

namespace UnoProgrammingTool
{
    public class Playthrough
    {

        public List<Key> Keys = new List<Key>();

        public float Length
        {
            get
            {
                if (Keys.Count < 1)
                    return 0;
                return Keys[Keys.Count - 1].Time;
            }
        }

        public Playthrough()
        {
            Clear();
        }

        public void AddKey(Key key)
        {
            var lastKeyIndex = GetLastKeyIndexAtTime(key.Time);
            Keys.Insert(lastKeyIndex + 1, key);
        }
        public void RemoveKey(int index)
        {
            Keys.RemoveAt(index);
        }
        public void Clear()
        {
            Keys.Clear();
            Keys.Add(new Key(0, 0.5f));
            Keys.Add(new Key(1, 0.5f));
        }
        public float Evaluate(float time)
        {
            if (Keys.Count <= 0)
                return 0;
            if (time < 0)
                return Keys[0].Position;
            var index = GetLastKeyIndexAtTime(time);
            if (index < Keys.Count - 1)
            {
                var key1 = Keys[index];
                var key2 = Keys[index + 1];
                var relativeTime = time - key1.Time;
                var maxRelativeTime = key2.Time - key1.Time;
                float alpha = (relativeTime / maxRelativeTime);
                return MathHelper.Lerp(key1.Position, key2.Position, alpha);
            }
            return Keys[Keys.Count - 1].Position;
        }

        public int GetLastKeyIndexAtTime(float time)
        {
            if (Keys.Count < 1) return -1;
            for(int i = 0; i < Keys.Count; i++)
            {
                var index = Keys.Count - i - 1;
                if (time >= Keys[index].Time)
                    return index;
            }
            return -1;
        }
    }

    public struct Key
    {
        public float Time { get; set; }
        public float Position { get; set; }
        public Key(float time, float position)
        {
            Time = time;
            Position = position;
        }
    }
}
