﻿
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace UnoProgrammingTool
{
    public static class PlaythroughDrawer
    {
        private static PathGradientBrush _keyBrush;

        static PlaythroughDrawer()
        {
            GraphicsPath path = new GraphicsPath();
            path.AddEllipse(0, 0, ChartDrawer.KeyRadius * 2, ChartDrawer.KeyRadius * 2);


            // Use the path to construct a brush.
            _keyBrush = new PathGradientBrush(path);

            // Set the color at the center of the path to blue.
            _keyBrush.CenterColor = Color.FromArgb(255, 0, 0, 255);

            // Set the color along the entire boundary 
            // of the path to aqua.
            Color[] colors = { Color.FromArgb(255, 0, 255, 255) };
            _keyBrush.SurroundColors = colors;
        }

        public static void Draw(double time, Playthrough playthrough, ChartViewport viewport, EditorInputStatus inputStatus)
        {
            DrawLines(time, playthrough, viewport);
            DrawKeyHandles(time, playthrough, viewport, inputStatus);
        }

        private static void DrawLines(double time, Playthrough playthrough, ChartViewport viewport)
        {
            DrawTools.Pen.Width = ChartDrawer.PlaythroughLineWidth;
            DrawTools.Pen.Color = ChartDrawer.PlaythroughLineColor;
            for (int i = 0; i < playthrough.Keys.Count - 1; i++)
            {
                DrawTools.Graphics.DrawLine(DrawTools.Pen,
                    viewport.KeyToPoint(playthrough.Keys[i], time),
                    viewport.KeyToPoint(playthrough.Keys[i + 1], time));
            }
            DrawTools.Pen.Width = 1;
        }


        private static void DrawKeyHandles(double time, Playthrough playthrough, ChartViewport viewport, EditorInputStatus inputStatus)
        {
            int radius = ChartDrawer.KeyRadius;
            for (int i = 0; i < playthrough.Keys.Count; i++)
            {
                var point = viewport.KeyToPoint(playthrough.Keys[i], time);
                var rect = new Rectangle(point.X - radius, point.Y - radius, radius * 2, radius * 2);
                if (!inputStatus.HoveringLine && inputStatus.KeyHoverIndex == i)
                {
                    DrawTools.Brush.Color = inputStatus.IsAltDown ? Color.Red : Color.Yellow;
                }
                else
                    DrawTools.Brush.Color = ChartDrawer.KeyColor;
                DrawTools.Graphics.FillEllipse(DrawTools.Brush, rect);
            }
            if (inputStatus.HoveringLine)
            {
                var rect = new Rectangle(inputStatus.LineIntersectPoint.X - radius, inputStatus.LineIntersectPoint.Y - radius, radius * 2, radius * 2);
                DrawTools.Brush.Color = ChartDrawer.KeyColor;
                DrawTools.Graphics.FillEllipse(DrawTools.Brush, rect);
            }
        }
    }
}
