﻿using System;
using System.Drawing;

namespace UnoProgrammingTool
{
    public static class ChartGridDrawer
    {

        public static void Draw(double time, ChartViewport viewport)
        {
            DrawTools.Pen.Width = 1;
            DrawVerticalLines(viewport.EdgeSpace, 1f - viewport.EdgeSpace, 5, viewport);
            DrawSecondLines(time, viewport);
            DrawTimeLine(viewport);
        }

        private static void DrawVerticalLines(float start, float end, int amount, ChartViewport viewport)
        {
            if (amount < 2) return;
            float spacing = (end - start) / (amount - 1);
            for (int i = 0; i < amount; i++)
            {
                DrawVerticalLine(start + i * spacing, viewport.Bounds.Size, Color.Gray);
            }
        }

        private static void DrawTimeLine(ChartViewport viewport)
        {
            DrawHorizontalLine(viewport.TimeLinePosition, viewport.Bounds.Size, Color.Red);
        }
        private static void DrawSecondLines(double time, ChartViewport viewport)
        {
            int lines = (int)Math.Floor(1f / viewport.SecondSpacing) + 1;
            float startTime = (float)time % 1f;
            for (int i = 0; i < lines; i++)
            {
                float y = viewport.TimeLinePosition;
                y += startTime * viewport.SecondSpacing;
                y -= viewport.SecondSpacing * i;
                y *= viewport.Bounds.Height;
                DrawTools.Pen.Color = Color.LightGray;
                var p1 = new Point(MathHelper.RoundToInt(viewport.Bounds.Width * viewport.EdgeSpace), MathHelper.RoundToInt(y));
                var p2 = new Point(MathHelper.RoundToInt(viewport.Bounds.Width - viewport.Bounds.Width * viewport.EdgeSpace), MathHelper.RoundToInt(y));
                DrawTools.Graphics.DrawLine(DrawTools.Pen, p1, p2);
            }
        }

        private static void DrawVerticalLine(float position, Size size, Color color)
        {
            DrawTools.Pen.Color = color;
            var x = (int)(size.Width * position);
            var p1 = new Point(x, 0);
            var p2 = new Point(x, size.Height);
            DrawTools.Graphics.DrawLine(DrawTools.Pen, p1, p2);
        }
        private static void DrawHorizontalLine(float position, Size size, Color color)
        {
            DrawTools.Pen.Color = color;
            var y = (int)(size.Height * position);
            var p1 = new Point(0, y);
            var p2 = new Point(size.Width, y);
            DrawTools.Graphics.DrawLine(DrawTools.Pen, p1, p2);
        }
    }
}
