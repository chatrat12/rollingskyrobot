﻿
using System;
using System.Drawing;

namespace UnoProgrammingTool
{
    public static class ChartInfoDrawer
    {

        public static void Draw(double time, ChartViewport viewport)
        {
            DrawTime(time, viewport);
        }

        private static void DrawTime(double time, ChartViewport viewport)
        {
            DrawTools.Brush.Color = Color.Red;
            int x = MathHelper.RoundToInt(viewport.Bounds.Width * viewport.EdgeSpace);
            int y = MathHelper.RoundToInt(viewport.Bounds.Height * viewport.TimeLinePosition + DrawTools.BoldFont.Height * 0.5f);
            DrawTools.Graphics.DrawString(MathHelper.GetTimeFormat((float)time), DrawTools.BoldFont, DrawTools.Brush, new Point(x, y));
        }
    }
}
