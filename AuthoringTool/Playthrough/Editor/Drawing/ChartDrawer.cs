﻿
using System.Drawing;

namespace UnoProgrammingTool
{
    public static class ChartDrawer
    {
        public static Color PlaythroughLineColor { get; } = Color.Orange;
        public static int   PlaythroughLineWidth { get; } = 3;
        public static int   KeyRadius { get; }            = 6;
        public static Color KeyColor { get; }             = Color.Orange;
        public static Color KeyHoverColor { get; }        = Color.Yellow;
        public static Color KeyRemoveColor { get; }       = Color.Red;

        public static void Draw(double time, Playthrough playthrough, ChartViewport viewport, EditorInputStatus inputStatus)
        {
            ChartGridDrawer.Draw(time, viewport);
            ChartInfoDrawer.Draw(time, viewport);
            if (playthrough != null)
                PlaythroughDrawer.Draw(time, playthrough, viewport, inputStatus);
        }
    }
}
