﻿
using System.Drawing;

namespace UnoProgrammingTool
{
    public static class DrawTools
    {
        public static SolidBrush Brush { get; private set; } = new SolidBrush(Color.White);
        public static Pen Pen { get; private set; } = new Pen(Color.White);
        public static Graphics Graphics { get; set; } = null;
        public static Font BoldFont { get; private set; } = new Font(FontFamily.GenericSansSerif, 24, FontStyle.Bold, GraphicsUnit.Pixel);

    }
}
