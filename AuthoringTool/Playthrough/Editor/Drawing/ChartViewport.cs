﻿
using System;
using System.Drawing;

namespace UnoProgrammingTool
{
    public class ChartViewport
    {
        const float TIME_LINE_POSITION = 0.85f;
        const float EDGE_SPACING = 0.05f;

        public Rectangle Bounds { get; set; }
        public int Zoom { get; set; } = 5;
        public float EdgeSpace { get { return EDGE_SPACING; } }
        public float TimeLinePosition { get { return TIME_LINE_POSITION; } }
        public float SecondSpacing { get { return 0.5f / 10 * Zoom; } }

        public Point KeyToPoint(Key key, double currentTime)
        {
            return new Point(PositionToPixel(key.Position), TimeToPixel(key.Time, currentTime));
        }
        public Key PointToKey(Point point, double currentTime)
        {
            return new Key(PixelToTime(point.Y, currentTime), PixelToPosition(point.X));
        }
        public float PixelToTime(int pixel, double currentTime)
        {
            float time = (float)(-pixel + Bounds.Height) - (1f - TIME_LINE_POSITION) * Bounds.Height;
            time = time / Bounds.Height / SecondSpacing;
            time += (float)currentTime;
            return time;
        }
        private float PixelToPosition(int pixel)
        {
            float position = (float)pixel - Bounds.Width * EDGE_SPACING;
            position = position / (Bounds.Width - Bounds.Width * EDGE_SPACING * 2);
            return Math.Min(Math.Max(position, 0), 1);
        }
        private int PositionToPixel(float position)
        {
            return MathHelper.RoundToInt(((1f - EDGE_SPACING * 2) * position + EDGE_SPACING) * Bounds.Width);
        }
        private int TimeToPixel(float time, double currentTime)
        {
            return MathHelper.RoundToInt((TIME_LINE_POSITION - time * SecondSpacing + (float)currentTime * SecondSpacing) * Bounds.Height);
        }
        

    }
}
