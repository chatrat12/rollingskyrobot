﻿

using System.Drawing;

namespace UnoProgrammingTool
{
    public class Chart
    {
        public PlaythroughFile File { get; set; } = new PlaythroughFile();
        public double Time { get; set; } = 0;
        public ChartViewport Viewport { get; private set; } = new ChartViewport();

        public void UpdateViewport(Rectangle bounds)
        {
            Viewport.Bounds = bounds;
        }
    }
}
