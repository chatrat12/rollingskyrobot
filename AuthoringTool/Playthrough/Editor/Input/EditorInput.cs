﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UnoProgrammingTool
{
    public class EditorInput
    {
        public event EventHandler RefreshRequested;

        private Point _previousMousePosition = Point.Empty;
        private Point _cursorPosition = Point.Empty;
        bool _movingKey = false;

        public EditorInput()
        {
        }

        public void OnMouseDown(MouseEventArgs e, Chart chart, EditorInputStatus inputStatus)
        {
            if (e.Button == MouseButtons.Right)
            {
                _previousMousePosition = e.Location;
            }
            if (e.Button == MouseButtons.Left)
            {
                LeftMouseButtonDown(chart, inputStatus);
            }
        }
        public void OnMouseUp(MouseEventArgs e, Chart chart, EditorInputStatus inputStatus)
        {
            if (e.Button == MouseButtons.Left)
            {
                _movingKey = false;
                UpdateHoverState(chart, inputStatus);
            }
        }
        public void OnMouseMove(MouseEventArgs e, Chart chart, EditorInputStatus inputStatus)
        {
            _cursorPosition = e.Location;
            if (e.Button == MouseButtons.Right)
            {
                RightMouseButtonMove(chart);
            }
            if (_movingKey)
                MoveKey(chart, inputStatus);
            else
                UpdateHoverState(chart, inputStatus);
        }

        private void RequestRefresh()
        {
            if (RefreshRequested != null)
                RefreshRequested(this, EventArgs.Empty);
        }

        private void LeftMouseButtonDown(Chart chart, EditorInputStatus inputStatus)
        {
            var playthrough = chart.File.Playthrough;
            var viewport = chart.Viewport;
            if (inputStatus.IsHoveringKey)
            {
                if (inputStatus.IsAltDown)
                {
                    if (playthrough.Keys.Count > 2 && inputStatus.KeyHoverIndex > 0)
                    {
                        playthrough.RemoveKey(inputStatus.KeyHoverIndex);
                        UpdateHoverState(chart, inputStatus);
                        chart.File.Dirty = true;
                    }
                }
                else
                {
                    if (inputStatus.HoveringLine)
                    {
                        playthrough.AddKey(viewport.PointToKey(_cursorPosition, chart.Time));
                        inputStatus.KeyHoverIndex++;
                        inputStatus.HoveringLine = false;
                        chart.File.Dirty = true;
                    }
                    _movingKey = true;
                }
            }
            else if (playthrough.Keys.Count > 0)
            {
                // if cursor position is above last key, add a new key
                if (viewport.PixelToTime(_cursorPosition.Y, chart.Time) > playthrough.Keys[playthrough.Keys.Count - 1].Time)
                {
                    playthrough.AddKey(viewport.PointToKey(_cursorPosition, chart.Time));
                    inputStatus.KeyHoverIndex = playthrough.Keys.Count - 1;
                    _movingKey = true;
                    chart.File.Dirty = true;
                }
            }
        }
        private void RightMouseButtonMove(Chart chart)
        {
            var playthrough = chart.File.Playthrough;
            chart.Time -= (float)(_previousMousePosition.Y - _cursorPosition.Y) / chart.Viewport.Bounds.Height / chart.Viewport.SecondSpacing;
            chart.Time = Math.Max(chart.Time, 0);
            if (playthrough.Keys.Count > 0)
                chart.Time = Math.Min(chart.Time, playthrough.Keys[playthrough.Keys.Count - 1].Time);
            _previousMousePosition = _cursorPosition;
        }
        private void MoveKey(Chart chart, EditorInputStatus inputStatus)
        {
            var keys = chart.File.Playthrough.Keys;
            var newKey = chart.Viewport.PointToKey(_cursorPosition, chart.Time);
            if (inputStatus.KeyHoverIndex == 0)
                newKey.Time = 0;
            if (inputStatus.KeyHoverIndex < keys.Count - 1)
                newKey.Time = Math.Min(newKey.Time, keys[inputStatus.KeyHoverIndex + 1].Time);
            if (inputStatus.KeyHoverIndex > 0)
                newKey.Time = Math.Max(newKey.Time, keys[inputStatus.KeyHoverIndex - 1].Time);
            if (inputStatus.SnapKeys)
                newKey.Position = (float)Math.Round(newKey.Position * 40) / 40f;
            keys[inputStatus.KeyHoverIndex] = newKey;
            chart.File.Dirty = true;
        }

        private void UpdateHoverState(Chart chart, EditorInputStatus inputStatus)
        {
            inputStatus.HoveringLine = false;
            UpdateHoverKey(chart, inputStatus);
            if (inputStatus.KeyHoverIndex < 0)
                UpdateHoverLine(chart, inputStatus);
        }
        private void UpdateHoverKey(Chart chart, EditorInputStatus inputStatus)
        {
            var newHoverKey = GetHoverKey(chart);
            if (newHoverKey != inputStatus.KeyHoverIndex)
            {
                inputStatus.KeyHoverIndex = newHoverKey;
                RequestRefresh();
            }
        }
        private void UpdateHoverLine(Chart chart, EditorInputStatus inputStatus)
        {
            var playthrough = chart.File.Playthrough;
            for (int i = 0; i < playthrough.Keys.Count - 1; i++)
            {
                var p1 = chart.Viewport.KeyToPoint(playthrough.Keys[i], chart.Time);
                var p2 = chart.Viewport.KeyToPoint(playthrough.Keys[i + 1], chart.Time);
                var v1 = new Vector2(p1.X, p1.Y);
                var v2 = new Vector2(p2.X, p2.Y);
                var cursorVector = new Vector2(_cursorPosition.X, _cursorPosition.Y);
                Vector2 intersection = new Vector2();
                if (Geometry.LineUnderPoint(cursorVector, v1, v2, 7, out intersection))
                {
                    inputStatus.KeyHoverIndex = i;
                    inputStatus.HoveringLine = true;
                    inputStatus.LineIntersectPoint = new Point(MathHelper.RoundToInt(intersection.X), MathHelper.RoundToInt(intersection.Y));
                }
            }
        }
        private int GetHoverKey(Chart chart)
        {
            var playthrough = chart.File.Playthrough;
            for (int i = 0; i < playthrough.Keys.Count; i++)
            {
                var point = chart.Viewport.KeyToPoint(playthrough.Keys[i], chart.Time);
                if (point.Distance(_cursorPosition) < ChartDrawer.KeyRadius)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
