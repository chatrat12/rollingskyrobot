﻿

using System.Drawing;
using System.Windows.Forms;

namespace UnoProgrammingTool
{
    public class EditorInputStatus
    {
        public int KeyHoverIndex { get; set; } = -1;
        public bool IsHoveringKey { get { return KeyHoverIndex >= 0; } }
        public bool HoveringLine { get; set; } = false;
        public Point LineIntersectPoint { get; set; }
        public bool IsAltDown { get { return (_modifierKeys & Keys.Alt) == Keys.Alt; } }
        public bool SnapKeys { get; set; } = true;

        private Keys _modifierKeys = Keys.None;

        public void UpdateModifierKeys(Keys keys)
        {
            _modifierKeys = keys;
        }
    }
}
