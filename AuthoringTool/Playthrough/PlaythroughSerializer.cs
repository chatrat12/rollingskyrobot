﻿using System.IO;

namespace UnoProgrammingTool
{
    public static class PlaythroughSerializer
    {
        private const int SAVE_VERSION = 0;
        public static byte[] Serialize(Playthrough playthrough)
        {
            var stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(SAVE_VERSION);
            writer.Write(playthrough.Keys.Count);
            foreach (var key in playthrough.Keys)
            {
                writer.Write(key.Time);
                writer.Write(key.Position);
            }
            return stream.ToArray();
        }
    }
}
