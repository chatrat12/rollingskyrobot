﻿
namespace UnoProgrammingTool
{
    public class PlaythroughFile
    {
        public Playthrough Playthrough
        {
            get { return _playthrough; }
            set
            {
                _playthrough = value;
            }
        }
        public string CurrentPath { get; set; } = string.Empty;
        public bool Dirty { get; set; } = false;

        Playthrough _playthrough = new Playthrough();
    }
}
