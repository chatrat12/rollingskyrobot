﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

namespace UnoProgrammingTool
{
    public partial class PlaythroughEditor : UserControl
    {
        public event EventHandler Stopped;

        public bool Playing { get; private set; } = false;
        public bool Snapping { get { return _inputStatus.SnapKeys; } set { _inputStatus.SnapKeys = value; } }
        public double Time { get { return Chart.Time; } set { Chart.Time = value; } }
        public Chart Chart { get; private set; } = new Chart();
        private EditorInput _input = new EditorInput();
        private EditorInputStatus _inputStatus = new EditorInputStatus();


        private Playthrough _playthrough { get { return Chart.File.Playthrough; } }



        public PlaythroughEditor()
        {
            InitializeComponent();
            this.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer,
                true);
        }
        public void SetPlaythroughFile(PlaythroughFile file)
        {
            Chart.File = file;
        }
        public void Play()
        {
            Time = 0;
            Playing = true;
            _inputStatus.KeyHoverIndex = 0;
            _inputStatus.HoveringLine = false;

        }
        public void Stop()
        {
            Playing = false;
            if (Stopped != null)
                Stopped(this, null);
        }
        public void Update(double deltaTime)
        {
            if (Playing)
            {
                Time += deltaTime;
                if (Time >= _playthrough.Length)
                {
                    Stop();
                    Time = _playthrough.Length;
                }
                Refresh();
            }
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (Playing) return;
            _input.OnMouseDown(e, Chart, _inputStatus);
            base.OnMouseDown(e);
            Refresh();
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (Playing) return;
            _input.OnMouseUp(e, Chart, _inputStatus);
            Refresh();
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (Playing) return;
            _input.OnMouseMove(e, Chart, _inputStatus);
            Refresh();
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            _inputStatus.UpdateModifierKeys(ModifierKeys);
            Refresh();
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            _inputStatus.UpdateModifierKeys(ModifierKeys);
            Refresh();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if (DrawTools.Graphics != e.Graphics)
                DrawTools.Graphics = e.Graphics;

            Clear(e.Graphics, Color.LightSkyBlue);
            Chart.Viewport.Bounds = Bounds;
            ChartDrawer.Draw(Time, _playthrough, Chart.Viewport, _inputStatus);
        }
        private void Clear(Graphics graphics, Color color)
        {
            DrawTools.Brush.Color = color;
            graphics.FillRectangle(DrawTools.Brush, new Rectangle(0, 0, Bounds.Width, Bounds.Height));
        }

        private void DrawServoPosition()
        {
            DrawTools.Brush.Color = Color.Black;
            var pos = new Point(10, Bounds.Height - DrawTools.BoldFont.Height - 5);
            DrawTools.Graphics.DrawString("Servo Position: " + _playthrough.Evaluate((float)Time), DrawTools.BoldFont, DrawTools.Brush, pos);
        }
    }
}
