﻿using System;
using System.Windows.Forms;

namespace UnoProgrammingTool.Controls
{
    public partial class ServoPanel : UserControl
    {
        public ServoStatus ServoStatus { get { return _servoStatus; } set { SetServoStatus(value); } }
        private ServoStatus _servoStatus = new ServoStatus();

        private const string DEGREE_FORMAT = "{0}°";
        public ServoPanel()
        {
            InitializeComponent();
            _visualizer.Paint += delegate { UpdateLabels(); };
            _visualizer.ServoLimitSet += _visualizer_ServoLimitSet;
        }

        private void _visualizer_ServoLimitSet(object sender, ServoVisualizer.HandleType e)
        {
            Properties.Settings.Default.MinServo = ServoStatus.MinAngle;
            Properties.Settings.Default.MaxServo = ServoStatus.MaxAngle;
            Properties.Settings.Default.Save();
        }

        private void SetServoStatus(ServoStatus servoStatus)
        {
            _servoStatus = servoStatus;
            _visualizer.ServoStatus = servoStatus;

        }
        private void UpdateLabels()
        {
            _minLabel.Text = FormatAngle(ServoStatus.MinAngle);
            _maxLabel.Text = FormatAngle(ServoStatus.MaxAngle);
            _currentLabel.Text = FormatAngle(ServoStatus.Angle);
        }
        private string FormatAngle(float angle)
        {
            return string.Format(DEGREE_FORMAT, Math.Round(angle, 1));
        }
    }
}
