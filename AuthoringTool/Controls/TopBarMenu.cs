﻿using System;
using System.IO;
using System.Windows.Forms;

namespace UnoProgrammingTool.Controls
{
    public partial class TopBarMenu : MenuStrip
    {
        public PlaythroughFile File { get; set; }
        private Playthrough _playthrough { get { return File.Playthrough; } }

        public TopBarMenu()
        {
            InitializeComponent();
            ToolStripMenuItem file = new ToolStripMenuItem("File");
            this.Items.Add(file);

            var _new = new ToolStripMenuItem("New");
            var open = new ToolStripMenuItem("Open");
            var save = new ToolStripMenuItem("Save");
            _new.Click += NewPlaythrough;
            open.Click += OpenPlaythrough;
            save.Click += SavePlaythrough;
            file.DropDownItems.Add(_new);
            file.DropDownItems.Add(open);
            file.DropDownItems.Add(save);

            file.DropDownItems.Add(new ToolStripSeparator());

            var exit = new ToolStripMenuItem("Exit");
            exit.Click += ExitApplication;
            file.DropDownItems.Add(exit);
        }

        public void SetPlaythroughFile(PlaythroughFile file)
        {
            File = file;
        }

        private DialogResult AskToSave()
        {
            var result = MessageBox.Show("Would you like to save current playthrough", "Save", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
                SavePlaythrough();
            return result;
        }
        private void SavePlaythrough()
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "Rolling Sky Playthrough (*.rsp)|*.rsp";
            sfd.SupportMultiDottedExtensions = false;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var binary = PlaythroughSerializer.Serialize(File.Playthrough);
                System.IO.File.WriteAllBytes(sfd.FileName, binary);
                File.Dirty = false;
            }
        }
        private void OpenPlaythrough()
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "Rolling Sky Playthrough (*.rsp)|*.rsp";
            ofd.Multiselect = false;
            ofd.SupportMultiDottedExtensions = false;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (System.IO.File.Exists(ofd.FileName))
                {
                    File.Playthrough = PlaythroughDeserializer.Dersialize(System.IO.File.ReadAllBytes(ofd.FileName));
                    File.Dirty = false;
                }
            }
        }

        private void NewPlaythrough(object sender, EventArgs e)
        {
            if (File.Dirty)
            {
                var result = AskToSave();
                if (result == DialogResult.Yes || result == DialogResult.No)
                    File.Playthrough.Clear();
            }
            else
                File.Playthrough.Clear();
        }
        private void OpenPlaythrough(object sender, EventArgs e)
        {
            if (File.Dirty)
            {
                var result = AskToSave();
                if (result == DialogResult.Yes || result == DialogResult.No)
                    OpenPlaythrough();
            }
            else
                OpenPlaythrough();
        }
        private void SavePlaythrough(object sender, EventArgs e)
        {
            SavePlaythrough();
        }
        private void ExitApplication(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
