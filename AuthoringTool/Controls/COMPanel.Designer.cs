﻿namespace UnoProgrammingTool.Controls
{
    partial class COMPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._bToggleSerial = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._cbBaud = new System.Windows.Forms.ComboBox();
            this._portStatusLabel = new System.Windows.Forms.Label();
            this._cbCOM = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._bToggleSerial);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._cbBaud);
            this.groupBox2.Controls.Add(this._portStatusLabel);
            this.groupBox2.Controls.Add(this._cbCOM);
            this.groupBox2.Location = new System.Drawing.Point(1, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 103);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Port";
            // 
            // _bToggleSerial
            // 
            this._bToggleSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._bToggleSerial.Location = new System.Drawing.Point(162, 73);
            this._bToggleSerial.Name = "_bToggleSerial";
            this._bToggleSerial.Size = new System.Drawing.Size(75, 23);
            this._bToggleSerial.TabIndex = 30;
            this._bToggleSerial.Text = "Open";
            this._bToggleSerial.UseVisualStyleBackColor = true;
            this._bToggleSerial.Click += new System.EventHandler(this._bToggleSerial_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Baud";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "COM";
            // 
            // _cbBaud
            // 
            this._cbBaud.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._cbBaud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbBaud.FormattingEnabled = true;
            this._cbBaud.Location = new System.Drawing.Point(69, 46);
            this._cbBaud.Name = "_cbBaud";
            this._cbBaud.Size = new System.Drawing.Size(168, 21);
            this._cbBaud.TabIndex = 20;
            // 
            // _portStatusLabel
            // 
            this._portStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._portStatusLabel.AutoSize = true;
            this._portStatusLabel.Location = new System.Drawing.Point(8, 78);
            this._portStatusLabel.Name = "_portStatusLabel";
            this._portStatusLabel.Size = new System.Drawing.Size(59, 13);
            this._portStatusLabel.TabIndex = 0;
            this._portStatusLabel.Text = "Port Status";
            // 
            // _cbCOM
            // 
            this._cbCOM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._cbCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbCOM.FormattingEnabled = true;
            this._cbCOM.Location = new System.Drawing.Point(69, 19);
            this._cbCOM.Name = "_cbCOM";
            this._cbCOM.Size = new System.Drawing.Size(168, 21);
            this._cbCOM.TabIndex = 10;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // COMPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Name = "COMPanel";
            this.Size = new System.Drawing.Size(246, 104);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _bToggleSerial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox _cbBaud;
        private System.Windows.Forms.Label _portStatusLabel;
        private System.Windows.Forms.ComboBox _cbCOM;
        private System.Windows.Forms.Timer timer1;
    }
}
