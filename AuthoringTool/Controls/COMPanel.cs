﻿using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;

namespace UnoProgrammingTool.Controls
{
    public partial class COMPanel : UserControl
    {
        private static int[] _baudRates = new int[]
        {
            300, 600, 1200, 2400, 4800, 9600,
            14400, 19200, 28800, 38400, 57600, 115200
        };

        public SerialPort Serial { get; private set; }

        public COMPanel()
        {
            InitializeComponent();
            Serial = new SerialPort();
            Serial.ReadTimeout = 500;
            Serial.WriteTimeout = 500;

            UpdateCOMComboBox();
            InitBaudComboBox();

            LoadSettings();
        }

        private void OpenPort()
        {
            if (_cbCOM.SelectedItem == null || _cbBaud.SelectedItem == null)
            {

                MessageBox.Show(string.Format("Invalid input!"));
                return;
            }
            var portName = (string)_cbCOM.SelectedItem;
            int baud = (int)_cbBaud.SelectedItem;
            if (!SerialPort.GetPortNames().Contains(portName))
            {
                MessageBox.Show(string.Format("Could not fine port name '{0}'.", portName));
                return;
            }
            Serial.PortName = portName;
            Serial.BaudRate = baud;
            Serial.ErrorReceived += ErrorReceived;

            Serial.DataReceived += Serial_DataReceived;

            Serial.Open();

            _cbCOM.Enabled = false;
            _cbBaud.Enabled = false;

            SaveSettings();
        }

        private void Serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            System.Console.WriteLine("A thing happened!");
        }

        private void ClosePort()
        {
            if (Serial.IsOpen)
                Serial.Close();
            _cbCOM.Enabled = true;
            _cbBaud.Enabled = true;
            UpdateCOMComboBox();
        }

        private void SaveSettings()
        {
            if (_cbCOM.SelectedItem != null)
                Properties.Settings.Default.COMName = (string)_cbCOM.SelectedItem;
            if (_cbBaud.SelectedItem != null)
                Properties.Settings.Default.BaudRate = (int)_cbBaud.SelectedItem;
            Properties.Settings.Default.Save();
        }

        private void LoadSettings()
        {
            for (int i = 0; i < _cbCOM.Items.Count; i++)
            {
                if ((string)_cbCOM.Items[i] == Properties.Settings.Default.COMName)
                    _cbCOM.SelectedIndex = i;
            }
            for (int i = 0; i < _cbBaud.Items.Count; i++)
            {
                if ((int)_cbBaud.Items[i] == Properties.Settings.Default.BaudRate)
                    _cbBaud.SelectedIndex = i;
            }
        }

        private void ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            MessageBox.Show(string.Format("Serial Error: '{0}'.", e.ToString()));
            ClosePort();
        }

        private void UpdateCOMComboBox()
        {
            var currentCOMName = _cbCOM.SelectedItem != null ? (string)_cbCOM.SelectedItem : string.Empty;
            _cbCOM.Items.Clear();
            var comNames = SerialPort.GetPortNames();
            for (int i = 0; i < comNames.Length; i++)
            {
                _cbCOM.Items.Add(comNames[i]);
                if (comNames[i] == currentCOMName)
                    _cbCOM.SelectedIndex = i;
            }
            if (_cbCOM.SelectedIndex < 0)
                _cbCOM.SelectedIndex = 0;
        }
        private void InitBaudComboBox()
        {
            foreach (var baudRate in _baudRates)
            {
                _cbBaud.Items.Add(baudRate);
            }
            _cbBaud.SelectedIndex = 5;
        }

        private void _bToggleSerial_Click(object sender, System.EventArgs e)
        {
            if (Serial.IsOpen)
            {
                _bToggleSerial.Text = "Open";
                ClosePort();
            }
            else
            {
                _bToggleSerial.Text = "Close";
                OpenPort();
            }
        }

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            if (!Serial.IsOpen && !_cbCOM.DroppedDown)
            {
                UpdateCOMComboBox();
            }
        }
    }
}
