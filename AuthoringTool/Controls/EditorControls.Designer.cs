﻿namespace UnoProgrammingTool.Controls
{
    partial class EditorControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._cbSnapping = new System.Windows.Forms.CheckBox();
            this._chartScaleSlider = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this._playButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chartScaleSlider)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._cbSnapping);
            this.groupBox1.Controls.Add(this._chartScaleSlider);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._playButton);
            this.groupBox1.Location = new System.Drawing.Point(1, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(319, 172);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Editor";
            // 
            // _cbSnapping
            // 
            this._cbSnapping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._cbSnapping.AutoSize = true;
            this._cbSnapping.Checked = true;
            this._cbSnapping.CheckState = System.Windows.Forms.CheckState.Checked;
            this._cbSnapping.Location = new System.Drawing.Point(242, 16);
            this._cbSnapping.Name = "_cbSnapping";
            this._cbSnapping.Size = new System.Drawing.Size(71, 17);
            this._cbSnapping.TabIndex = 20;
            this._cbSnapping.Text = "Snapping";
            this._cbSnapping.UseVisualStyleBackColor = true;
            this._cbSnapping.CheckedChanged += new System.EventHandler(this._cbSnapping_CheckedChanged);
            // 
            // _chartScaleSlider
            // 
            this._chartScaleSlider.Location = new System.Drawing.Point(13, 32);
            this._chartScaleSlider.Minimum = 1;
            this._chartScaleSlider.Name = "_chartScaleSlider";
            this._chartScaleSlider.Size = new System.Drawing.Size(104, 45);
            this._chartScaleSlider.TabIndex = 10;
            this._chartScaleSlider.Value = 5;
            this._chartScaleSlider.Scroll += new System.EventHandler(this._chartScaleSlider_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Chart Scale";
            // 
            // _playButton
            // 
            this._playButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._playButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._playButton.Location = new System.Drawing.Point(6, 92);
            this._playButton.Name = "_playButton";
            this._playButton.Size = new System.Drawing.Size(307, 74);
            this._playButton.TabIndex = 30;
            this._playButton.Text = "Play";
            this._playButton.UseVisualStyleBackColor = true;
            this._playButton.Click += new System.EventHandler(this._playButton_Click);
            // 
            // EditorControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "EditorControls";
            this.Size = new System.Drawing.Size(321, 173);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chartScaleSlider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _playButton;
        private System.Windows.Forms.CheckBox _cbSnapping;
        private System.Windows.Forms.TrackBar _chartScaleSlider;
        private System.Windows.Forms.Label label1;
    }
}
