﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UnoProgrammingTool.Controls
{
    public partial class ServoVisualizer : Control
    {
        public event EventHandler<HandleType> ServoLimitSet;

        private const float HANDLE_RADIUS = 6;
        private static Color HANDLE_COLOR = Color.DarkGray;
        private static Color HANDLE_HOVER_COLOR = Color.Gray;
        private static Color ARM_COLOR = Color.Black;
        private static Color HARD_LIMIT_COLOR = Color.LightGreen;
        private static Color LIMIT_COLOR = Color.Green;

        public ServoStatus ServoStatus { get { return _servoStatus; } set { SetServoStatus(value); } }
        private ServoStatus _servoStatus = new ServoStatus();


        private float _hardLimitMax = 45;
        private float _hardLimitMin = 5;

        private float _pirRadius { get { return Bounds.Height / 2; } }
        public float _armLength { get { return Bounds.Height * 0.6f; } }

        private HandleType _hoverHandle = HandleType.None;
        private bool _movingHandle = false;

        public ServoVisualizer()
        {
            InitializeComponent();
            this.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer,
                true);
        }
        private void SetServoStatus(ServoStatus servoStatus)
        {
            _servoStatus = servoStatus;
            if (servoStatus != null)
                _servoStatus.AngleUpdated += delegate { Refresh(); };
        }

        #region Input
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                if (CursorOverHandle(e.Location, HandleType.Arm))
                    _hoverHandle = HandleType.Arm;
                else if (CursorOverHandle(e.Location, HandleType.MinLimit))
                    _hoverHandle = HandleType.MinLimit;
                else if (CursorOverHandle(e.Location, HandleType.MaxLimit))
                    _hoverHandle = HandleType.MaxLimit;
                else
                    _hoverHandle = HandleType.None;

                _movingHandle = _hoverHandle != HandleType.None;
            }

        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (_movingHandle)
            {
                var angle = GetCursorAngle(e.Location);
                switch (_hoverHandle)
                {
                    case HandleType.Arm:
                        ServoStatus.Angle = MathHelper.Clamp(angle, ServoStatus.MinAngle, ServoStatus.MaxAngle);
                        break;
                    case HandleType.MinLimit:
                        ServoStatus.MinAngle = MathHelper.Clamp(angle, -_hardLimitMax, -_hardLimitMin);
                        ServoStatus.Angle = ServoStatus.MinAngle;
                        break;
                    case HandleType.MaxLimit:
                        ServoStatus.MaxAngle = MathHelper.Clamp(angle, _hardLimitMin, _hardLimitMax);
                        ServoStatus.Angle = ServoStatus.MaxAngle;
                        break;
                }
                Refresh();
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left)
                _movingHandle = false;
            if(_hoverHandle == HandleType.MinLimit || _hoverHandle == HandleType.MaxLimit)
            {
                if (ServoLimitSet != null)
                    ServoLimitSet(this, _hoverHandle);
            }
        }

        private float GetCursorAngle(Point cursorPosition)
        {
            var basePos = new Point(Bounds.Width / 2, Bounds.Height);
            float angle = (float)Math.Atan2(basePos.Y - cursorPosition.Y, basePos.X - cursorPosition.X);
            angle /= (float)Math.PI / 180f;
            angle -= 90;
            return angle;
        }

        private bool CursorOverHandle(Point cursorLocation, HandleType handle)
        {
            var pos = GetHandleLocation(handle);
            return cursorLocation.Distance(pos) < HANDLE_RADIUS;
        }

        #endregion

        #region Draw

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            pe.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            DrawAngleLimits(pe.Graphics);
            DrawArm(pe.Graphics, ARM_COLOR, 15, ServoStatus.Angle);
            DrawHandles(pe.Graphics);
        }

        private void DrawAngleLimits(Graphics graphics)
        {
            var rect = new Rectangle(0, Bounds.Height / 2, Bounds.Width, Bounds.Height);
            DrawPie(graphics, rect, HARD_LIMIT_COLOR, -_hardLimitMax, _hardLimitMax);
            DrawPie(graphics, rect, LIMIT_COLOR, ServoStatus.MinAngle, ServoStatus.MaxAngle);
        }
        private void DrawArm(Graphics graphics, Color color, float width, float angle)
        {

            DrawTools.Pen.Color = color;
            DrawTools.Pen.Width = width;
            DrawTools.Pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            angle = (angle - 90) * (float)Math.PI / 180f;

            float armHeight = Bounds.Height * 0.6f;

            var p1 = new Point(Bounds.Width / 2, Bounds.Height);
            var p2 = new Point((int)(p1.X + Math.Cos(angle) * armHeight), (int)(p1.Y + Math.Sin(angle) * armHeight));

            graphics.DrawLine(DrawTools.Pen, p1, p2);

        }

        private void DrawHandles(Graphics graphics)
        {
            DrawHandle(graphics, HandleType.Arm, HANDLE_RADIUS);
            DrawHandle(graphics, HandleType.MinLimit, HANDLE_RADIUS);
            DrawHandle(graphics, HandleType.MaxLimit, HANDLE_RADIUS);
        }

        private void DrawHandle(Graphics graphics, HandleType handle, float radius)
        {
            var pos = GetHandleLocation(handle);
            DrawTools.Brush.Color = _hoverHandle == handle ? HANDLE_HOVER_COLOR : HANDLE_COLOR;
            graphics.FillEllipse(DrawTools.Brush, pos.X - radius, pos.Y - radius, radius * 2, radius * 2);
        }

        private void DrawPie(Graphics graphics, Rectangle rect, Color color, float min, float max)
        {
            DrawTools.Brush.Color = color;
            float sweep = max - min;
            float angle = min - 90;
            graphics.FillPie(DrawTools.Brush, rect, angle, sweep);
        }
        #endregion

        private Point GetHandleLocation(HandleType handle)
        {
            if (handle == HandleType.None)
                return Point.Empty;
            float distance = 0f;
            float angle = 0f;
            switch (handle)
            {
                case HandleType.Arm:
                    angle = ServoStatus.Angle;
                    distance = _armLength;
                    break;
                case HandleType.MinLimit:
                    angle = ServoStatus.MinAngle;
                    distance = _pirRadius;
                    break;
                case HandleType.MaxLimit:
                    angle = ServoStatus.MaxAngle;
                    distance = _pirRadius;
                    break;
            }
            angle -= 90;
            angle *= (float)Math.PI / 180f;


            var pos = new Point(Bounds.Width / 2, Bounds.Height);
            return new Point((int)(pos.X + Math.Cos(angle) * distance), (int)(pos.Y + Math.Sin(angle) * distance));
        }

        public enum HandleType
        {
            None,
            Arm,
            MinLimit,
            MaxLimit
        }
    }
}
