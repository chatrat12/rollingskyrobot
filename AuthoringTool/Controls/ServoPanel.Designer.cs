﻿namespace UnoProgrammingTool.Controls
{
    partial class ServoPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this._minLabel = new System.Windows.Forms.Label();
            this._maxLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._currentLabel = new System.Windows.Forms.Label();
            this._visualizer = new UnoProgrammingTool.Controls.ServoVisualizer();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this._minLabel);
            this.panel1.Controls.Add(this._maxLabel);
            this.panel1.Controls.Add(this._visualizer);
            this.panel1.Location = new System.Drawing.Point(100, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 130);
            this.panel1.TabIndex = 1;
            // 
            // _minLabel
            // 
            this._minLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._minLabel.BackColor = System.Drawing.Color.Transparent;
            this._minLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._minLabel.Location = new System.Drawing.Point(3, 110);
            this._minLabel.Name = "_minLabel";
            this._minLabel.Size = new System.Drawing.Size(65, 20);
            this._minLabel.TabIndex = 2;
            this._minLabel.Text = "Min";
            // 
            // _maxLabel
            // 
            this._maxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._maxLabel.BackColor = System.Drawing.Color.Transparent;
            this._maxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._maxLabel.Location = new System.Drawing.Point(135, 110);
            this._maxLabel.Name = "_maxLabel";
            this._maxLabel.Size = new System.Drawing.Size(65, 20);
            this._maxLabel.TabIndex = 2;
            this._maxLabel.Text = "Max";
            this._maxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._currentLabel);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(1, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 188);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Servo";
            // 
            // _currentLabel
            // 
            this._currentLabel.AllowDrop = true;
            this._currentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._currentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._currentLabel.Location = new System.Drawing.Point(151, 25);
            this._currentLabel.Name = "_currentLabel";
            this._currentLabel.Size = new System.Drawing.Size(100, 20);
            this._currentLabel.TabIndex = 2;
            this._currentLabel.Text = "Angle";
            this._currentLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _visualizer
            // 
            this._visualizer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._visualizer.Location = new System.Drawing.Point(0, -70);
            this._visualizer.Name = "_visualizer";
            this._visualizer.Size = new System.Drawing.Size(200, 200);
            this._visualizer.TabIndex = 0;
            this._visualizer.Text = "servoVisualizer1";
            // 
            // ServoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ServoPanel";
            this.Size = new System.Drawing.Size(400, 189);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ServoVisualizer _visualizer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label _minLabel;
        private System.Windows.Forms.Label _maxLabel;
        private System.Windows.Forms.Label _currentLabel;
    }
}
