﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnoProgrammingTool.Controls
{
    public partial class EditorControls : UserControl
    {
        public PlaythroughEditor Editor
        {
            get { return _playthroughEditor; }
            set
            {
                _playthroughEditor = value;
                if (value != null)
                {
                    _playthroughEditor.Stopped += delegate
                    {
                        _playButton.Text = "Play";
                    };
                }
            }
        }
        private PlaythroughEditor _playthroughEditor;

        public EditorControls()
        {
            InitializeComponent();
        }

        private void _playButton_Click(object sender, EventArgs e)
        {
            if (!Editor.Playing)
            {
                Editor.Play();
                _playButton.Text = "Stop";
            }
            else
            {
                Editor.Stop();
                _playButton.Text = "Play";
            }
        }

        private void _cbSnapping_CheckedChanged(object sender, EventArgs e)
        {
            _playthroughEditor.Snapping = _cbSnapping.Checked;
        }

        private void _chartScaleSlider_Scroll(object sender, EventArgs e)
        {
            _playthroughEditor.Chart.Viewport.Zoom = _chartScaleSlider.Value;
        }
    }
}
