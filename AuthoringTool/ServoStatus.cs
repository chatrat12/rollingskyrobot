﻿

namespace UnoProgrammingTool
{
    public class ServoStatus
    {
        public event System.EventHandler AngleUpdated;
        public float Angle
        {
            get { return _angle; }
            set
            {
                _angle = value;
                if (AngleUpdated != null)
                    AngleUpdated(this, System.EventArgs.Empty);
            }
        }
        public float MinAngle { get; set; } = -20;
        public float MaxAngle { get; set; } = 20;

        private float _angle = 0;
    }
}
