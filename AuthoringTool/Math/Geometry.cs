﻿using System;


namespace UnoProgrammingTool
{
    public static class Geometry
    {
        public static bool VertexUnderPoint(Vector2 vertex, float vertexSize, Vector2 point)
        {
            return Vector2.Distance(vertex, point) <= vertexSize;
        }
        public static bool LineUnderPoint(Vector2 checkPoint, Vector2 linePoint1, Vector2 linePoint2, float lineWidth, out Vector2 intersection)
        {
            intersection = Vector2.Zero;
            if (!LineCapCheck(checkPoint, linePoint1, linePoint2))
                return false;

            intersection = GetIntersctionOfLineAndPoint(checkPoint, linePoint1, linePoint2);
            return Vector2.Distance(checkPoint, intersection) <= lineWidth;
        }

        public static bool LineUnderPoint(Vector2 checkPoint, Vector2 linePoint1, Vector2 linePoint2, float lineWidth)
        {
            if (LineCapCheck(checkPoint, linePoint1, linePoint2))
                return false;
            return PointDistanceFromLine(checkPoint, linePoint1, linePoint2) <= lineWidth;
        }
        public static bool CicleLineUnderPoint(Vector2 checkPoint, Vector2 cirlcePosition, float circleRadius, float circleLineWidth)
        {
            var halfLineWidth = circleLineWidth * 0.5f;
            var dist = Vector2.Distance(checkPoint, cirlcePosition);
            return dist >= circleRadius - halfLineWidth && dist <= circleRadius + halfLineWidth;
        }

        private static Vector2 GetIntersctionOfLineAndPoint(Vector2 checkPoint, Vector2 linePoint1, Vector2 linePoint2)
        {
            Vector2 targetVector = linePoint1 - checkPoint;
            Vector2 perpendicular = new Vector2(linePoint2.Y - linePoint1.Y, -(linePoint2.X - linePoint1.X));
            var scale = Vector2.Dot(targetVector, perpendicular) / perpendicular.Length();
            Vector2 intersection = Vector2.Normalize(perpendicular) * scale + checkPoint;
            return intersection;
        }

        private static float PointDistanceFromLine(Vector2 checkPoint, Vector2 linePoint1, Vector2 linePoint2)
        {
            double dist = Math.Abs((linePoint2.X - linePoint1.X) * (linePoint1.Y - checkPoint.Y)
                                 - (linePoint1.X - checkPoint.X) * (linePoint2.Y - linePoint1.Y));
            dist /= Math.Sqrt(Math.Pow((linePoint2.X - linePoint1.X), 2)
                            + Math.Pow((linePoint2.Y - linePoint1.Y), 2));
            return (float)dist;
        }
        private static bool LineCapCheck(Vector2 checkPoint, Vector2 linePoint1, Vector2 linePoint2)
        {
            // Check if point goes beyond end caps.
            // If they do, the point cannont be over the line
            var v1 = linePoint2 - linePoint1;
            var v2 = checkPoint - linePoint1;
            if (Vector2.Dot(v1, v2) < 0)
                return false;
            v1 = linePoint1 - linePoint2;
            v2 = checkPoint - linePoint2;
            if (Vector2.Dot(v1, v2) < 0)
                return false;
            return true;
        }
    }
}
