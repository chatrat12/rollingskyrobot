﻿
using System;
using System.Drawing;

namespace UnoProgrammingTool
{
    public static class MathHelper
    {
        public static float Lerp(float start, float end, float alpha)
        {
            return (start + alpha * (end - start));
        }

        public static int RoundToInt(float f)
        {
            return (int)Math.Round(f);
        }
        public static string GetTimeFormat(float time)
        {
            string minutes = Math.Floor(time / 60f).ToString().PadLeft(2, '0');
            string seconds = Math.Floor(time % 60f).ToString().PadLeft(2, '0');
            string milliseconds = Math.Round(time - Math.Truncate(time), 3).ToString();
            if (milliseconds.Length > 2)
                milliseconds = milliseconds.Substring(2, milliseconds.Length - 2).PadRight(3, '0');
            else
                milliseconds = "000";
            return string.Format("{0}:{1}.{2}", minutes, seconds, milliseconds);
        }
        public static float Distance(this Point point, Point otherPoint)
        {
            return (float)Math.Sqrt(Math.Pow(otherPoint.X - point.X, 2) + Math.Pow(otherPoint.Y - point.Y, 2));
        }
        public static float Clamp(float value, float min, float max)
        {
            value = Math.Max(min, value);
            return Math.Min(value, max);
        }
    }
}
